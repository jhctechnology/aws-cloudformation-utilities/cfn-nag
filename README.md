# cfn-nag expanded ruleset

Expands on [stelligent/cfn_nag](https://github.com/stelligent/cfn_nag). All credit for the orignal cfn-nag goes to thag project and team.

**If this is your first time visiting this project, check jhctechnology/aws-cloudformation-utilities/awslint> first for an introduction and contribution guidelines.**
